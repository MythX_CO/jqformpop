/**
 * jqForm accepts the following options:
 * 
 * dataFieldClassName	- Required - Identifies the class assigned to the elements that will receive data.
 * repeatDomClassName	- Optional - Identifies a repeating DOM element.
 * dataObject			- Required - This is the data that gets populated into the form
 * cbDataObjectBefore	- Optional - Callback run on element receiving data just prior.
 * cbDataObjectAfter	- Optional - Callback run on element receiving data just after.
 * 
 */
(function($){
	$.fn.jqForms = function(methodOrOptions){
		
		//Can only be applied to single jQuery element.
		if(this.length != 1){  
			console.log("jqForms only works with single jQuery element.");
			return this;
		}
		
		//Define options property
		//$.fn.jqForms.objOptions = {};
		
		//Define default settings
		$.fn.jqForms.objDefaults = {
			dataFieldClassName:'jq-forms-data',
			repeatDomClassName:'jq-forms-repeat',
			nesting:false
		};
		
		// Private Methods
		
		/**
		 * 
		 */
		var populateElement = function(domElem, objData){
			debugger;
		};
		
		/**
		 * 
		 */
		var populateList = function(domElem, objData){
			
		};
		
		//Define plugin methods
		var methods = {
			/**
			 * 
			 * Initial Method, runs at instantiation.
			 * 
			 */
			init:function(objOptions){
				this.instantiationStatus = true;
				this.objOptions = $.extend({},$.fn.jqForms.objDefaults,objOptions);
				return this;
			},
			/**
			 * Receives options and populates the form
			 * objOptions - receives additional options for this particular execution.
			 * 
			 */
			populateForm : function(objOptionsPassed) {
				debugger;
				var objOptions 			= $.extend({},this.objOptions,objOptionsPassed);
				var objData				= objOptions.dataObject;
				var domFields 			= this.find("."+this.objOptions.dataFieldClassName);
				
				//Do we have any fields to update?
				if(domFields.length < 1){
					console.log("No fields in form to populate.  populateForm() exiting.");
					return this;
				}
				
				//Iterate over fields
				domFields.each(jQuery.proxy(function(i,e){
					var domElem				= jQuery(e);
					
					//Run Pre Callback
					if (typeof(objOptions.cbDataObjectBefore) == "function")
						objOptions.cbDataObjectBefore(domElem,i);

					debugger;
					//Check if this is a repeating dom element.
					if(domElem.hasClass(this.objOptions.repeatDomClassName)){
						
					}
					populateElement(domElem,objData);
					
					//Run Post Callback
					if (typeof(objOptions.cbDataObjectAfter) == "function")
						objOptions.cbDataObjectBefore(domElem,i);
				},this));
			}
		};

		//Controller
		if ( methods[methodOrOptions] ) {
			//Run specified method
			return methods[methodOrOptions].apply(this,Array.prototype.slice.call(arguments,1));
		} else if (typeof methodOrOptions === 'object' || ! methodOrOptions ) {
			// Default to "init" method
			if(this.instantiationStatus) console.log("Already instantiated on this jQuery element");
			return methods.init.apply(this,arguments);
		} else {
			$.error( 'Method ' +  methodOrOptions + ' does not exist on jQuery.jqForms' );
		}
		return this;
	}
}(jQuery));